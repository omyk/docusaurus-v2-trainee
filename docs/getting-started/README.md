---
description: Start using Shotstack now.
---

# Getting Started

To quickly understand the basics of how a video edit is constructed using JSON, start here:

{% page-ref page="core-concepts.md" %}

You will need an API key before you can call the API:

{% page-ref page="request-api-keys.md" %}

Create your first video edit in 5 minutes using Curl:

{% page-ref page="hello-world-using-curl.md" %}

